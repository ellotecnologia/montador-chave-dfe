unit MainForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ACBrBase, ACBrDFe, ACBrNFe, ComCtrls, ShellCtrls, ACBrNFeWebServices,
  ExtCtrls;

type
  TChaveAcesso = record
    UF: String[2];
    Ano: String[2];
    Mes: String[2];
    CNPJ: String[14];
    Modelo: String[2];
    Serie: String[3];
    nNF: String[9];
    TpEmis: Char;
    cNF: String[8];
    DV: Char;
  end;

  TMainFrm = class(TForm)
    ACBrNFe1: TACBrNFe;
    Bevel1: TBevel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    lbAnoMes: TEdit;
    lbCNF: TEdit;
    lbCNPJ: TEdit;
    lbModelo: TEdit;
    lbNNF: TEdit;
    lbSerie: TEdit;
    lbTipoEmissao: TEdit;
    lbUF: TEdit;
    lbChaveAcesso: TRichEdit;
    procedure lbChaveAcessoChange(Sender: TObject);
    procedure AtualizaChaveAcesso(Sender: TObject);
  private
    function TodosCamposPreenchidos: Boolean;
    procedure LimpaCampos;
    procedure AtualizaCampos(Chave: TChaveAcesso);
    procedure AtualizaChaveExibida(Chave: TChaveAcesso);
  end;

var
  MainFrm: TMainFrm;

implementation

uses pcnAuxiliar, ACBrUtil, DateUtils, StringUtils;

{$R *.dfm}

function ExtraiChave(const Chave: String): TChaveAcesso;
begin
   Result.UF     := Copy(Chave, 1, 2);
   Result.Ano    := Copy(Chave, 3, 2);
   Result.Mes    := Copy(Chave, 5, 2);
   Result.CNPJ   := Copy(Chave, 7, 14);
   Result.Modelo := Copy(Chave, 21, 2);
   Result.Serie  := Copy(Chave, 23, 3);
   Result.nNF    := Copy(Chave, 26, 9);
   Result.TpEmis := Copy(Chave, 35, 1)[1];
   Result.cNF    := Copy(Chave, 36, 8);
   Result.DV     := Copy(Chave, 44, 1)[1];
end;

function ChaveParaStr(Chave: TChaveAcesso): String;
begin
   Result := Format('%s%s%s%s%s%s%s%s%s%s', [
      Chave.UF,
      Chave.Ano,
      Chave.Mes,
      Chave.CNPJ,
      Chave.Modelo,
      Chave.Serie,
      Chave.nNF,
      Chave.TpEmis,
      Chave.cNF,
      Chave.DV
   ]);
end;

{ TMainFrm }

procedure TMainFrm.lbChaveAcessoChange(Sender: TObject);
var Chave: TChaveAcesso;
begin
   LimpaCampos;

   if Length(lbChaveAcesso.Text) < 44 then Exit;

   Chave := ExtraiChave(lbChaveAcesso.Text);

   AtualizaChaveExibida(Chave);

   AtualizaCampos(Chave);
end;

procedure TMainFrm.LimpaCampos;
begin
    lbUF.Text          := '';
    lbAnoMes.Text      := '';
    lbCNPJ.Text        := '';
    lbModelo.Text      := '';
    lbSerie.Text       := '';
    lbNNF.Text         := '';
    lbTipoEmissao.Text := '';
    lbCNF.Text         := '';
end;

procedure TMainFrm.AtualizaCampos(Chave: TChaveAcesso);
begin
    lbUF.OnChange := nil;
    lbAnoMes.OnChange := nil;
    lbCNPJ.OnChange := nil;
    lbModelo.OnChange := nil;
    lbSerie.OnChange := nil;
    lbNNF.OnChange := nil;
    lbTipoEmissao.OnChange := nil;
    lbCNF.OnChange := nil;

    lbUF.Text          := Chave.UF;
    lbAnoMes.Text      := Chave.Ano + Chave.Mes;
    lbCNPJ.Text        := Chave.CNPJ;
    lbModelo.Text      := Chave.Modelo;
    lbSerie.Text       := Chave.Serie;
    lbNNF.Text         := chave.nNF;
    lbTipoEmissao.Text := chave.TpEmis;
    lbCNF.Text         := chave.cNF;

    lbUF.OnChange := AtualizaChaveAcesso;
    lbAnoMes.OnChange := AtualizaChaveAcesso;
    lbCNPJ.OnChange := AtualizaChaveAcesso;
    lbModelo.OnChange := AtualizaChaveAcesso;
    lbSerie.OnChange := AtualizaChaveAcesso;
    lbNNF.OnChange := AtualizaChaveAcesso;
    lbTipoEmissao.OnChange := AtualizaChaveAcesso;
    lbCNF.OnChange := AtualizaChaveAcesso;
end;

procedure TMainFrm.AtualizaChaveAcesso(Sender: TObject);
var
   Digito: Integer;
   Chave: TChaveAcesso;
   NovaChave: String;
begin
   if not TodosCamposPreenchidos then Exit;

   Chave.UF     := StrZero(lbUF.Text, 2);
   Chave.Ano    := Copy(lbAnoMes.Text, 1, 2);
   Chave.Mes    := Copy(lbAnoMes.Text, 3, 2);
   Chave.CNPJ   := lbCNPJ.Text;
   Chave.Modelo := StrZero(lbModelo.Text, 2);
   Chave.Serie  := StrZero(lbSerie.Text, 3);
   Chave.nNF    := StrZero(lbNNF.Text, 9);
   Chave.TpEmis := StrZero(lbTipoEmissao.Text, 1)[1];
   Chave.cNF    := StrZero(lbCNF.Text, 8);
   Chave.DV     := '0';

   NovaChave := ChaveParaStr(Chave);

   GerarDigito(Digito, Copy(NovaChave, 1, 43));

   Chave.DV := IntToStr(Digito)[1];

   NovaChave := ChaveParaStr(Chave);

   lbChaveAcesso.OnChange := nil;
//   lbChaveAcesso.Text := NovaChave;
   AtualizaChaveExibida(Chave);
   lbChaveAcesso.OnChange := lbChaveAcessoChange;
end;

function TMainFrm.TodosCamposPreenchidos: Boolean;
begin
   Result := (Length(lbUF.Text) > 0)
      and (Length(lbAnoMes.Text) > 0)
      and (Length(lbCNPJ.Text) > 0)
      and (Length(lbModelo.Text) > 0)
      and (Length(lbSerie.Text) > 0)
      and (Length(lbNNF.Text) > 0)
      and (Length(lbTipoEmissao.Text) > 0)
      and (Length(lbCNF.Text) > 0);
end;

procedure TMainFrm.AtualizaChaveExibida(Chave: TChaveAcesso);
begin
   lbChaveAcesso.Clear;
   lbChaveAcesso.SelAttributes.Color := $006F47EF;
   lbChaveAcesso.SelText := Chave.UF;

   lbChaveAcesso.SelAttributes.Color := $0066D1FF;
   lbChaveAcesso.SelText := Chave.Ano;

   lbChaveAcesso.SelAttributes.Color := $00A0D606;
   lbChaveAcesso.SelText := Chave.Mes;

   lbChaveAcesso.SelAttributes.Color := $00B28A11;
   lbChaveAcesso.SelText := Chave.CNPJ;

   lbChaveAcesso.SelAttributes.Color := $004C3B07;
   lbChaveAcesso.SelText := Chave.Modelo;

   lbChaveAcesso.SelAttributes.Color := $006F47EF;
   lbChaveAcesso.SelText := Chave.Serie;

   lbChaveAcesso.SelAttributes.Color := $0066D1FF;
   lbChaveAcesso.SelText := Chave.nNF;

   lbChaveAcesso.SelAttributes.Color := $00A0D606;
   lbChaveAcesso.SelText := Chave.TpEmis;

   lbChaveAcesso.SelAttributes.Color := $00B28A11;
   lbChaveAcesso.SelText := Chave.cNF;

   lbChaveAcesso.SelAttributes.Color := $004C3B07;
   lbChaveAcesso.SelText := Chave.DV;
end;

end.

